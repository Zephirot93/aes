from pyaes import AESModeOfOperationCTR
from base64 import b64encode, b64decode
import sys


HELP = "Uso: aes.py [encrypt|decrypt] [llave (128 bits)] [mensaje]" 

def main():
	args = sys.argv[1:]
	if args[0] not in ["encrypt", "decrypt"]:
		print(HELP)
		sys.exit(1)
	try:
		llave, mensaje = args[-2], args[-1]
		aes = AESModeOfOperationCTR(llave.encode())
		if args[0] == "encrypt":
			cipher = aes.encrypt(mensaje)
			print(b64encode(cipher).decode("utf-8"))
		if args[0] == "decrypt":
			cipher = b64decode(mensaje)
			print(aes.decrypt(cipher).decode("utf-8"))
	except IndexError:
		print(HELP)
		sys.exit(1)

if __name__ == "__main__":
	main()
